# backupninja
#
# This is the main class, which contains all other classes in this module.
#
# @param configfile
#   The backupninja main configuration file.
#   Default value: '/etc/backupninja.conf'
#
# @param configdirectory
#   The directory where all the backup action configuration files live.
#   Default value: '/etc/backup.d'
#
# @param purge_configdirectory
#   Whether to purge unmanaged files in the backup action directory.
#   Default value: false
#
# @param executable
#   The path to the backupninja executable.
#   Default value: '/usr/sbin/backupninja'
#
# @param when
#   When to process backup action configuration files.
#   Default value: 'everyday at 01:00'
#
# @param loglvl
#   How verbose to make the backupninja logs (1-5).
#   Default value: 4
#
# @param logfile
#   Where backupninja should put its log messages.
#   Default value: '/var/log/backupninja.log'
#
# @param scriptdirectory
#   Where backupninja handler scripts are found.
#   Default value: '/usr/share/backupninja'
#
# @param libdirectory
#   Where the backupninja libraries are found.
#   Default value: '/usr/lib/backupninja'
#
# @param usecolors
#   Whether to use colors in the log file and debug output.
#   Default value: true
#
# @param reportinfo
#   Whether to send an email report when a backup action emits an informational
#   message.
#   Default value: false
#
# @param reportemail
#   Send a summary of the backup status to this email address.
#   Default value: 'root'
#
# @param reportsuccess
#   Whether to send an email report in case of backup action success.
#   Default value: true
#
# @param reportwarning
#   Whether to send an email report when a backup action emits a warning.
#   Default value: true
#
# @param reportspace
#   Whether to include disk space usage is backup email report.
#   Default value: false
#
# @param reporthost
#   Where to rsync the backupninja.log to be agregated in a ninjareport.
#   Default value: undef
#
# @param reportuser
#   What user to connect to reporthost to sync the backupninja.log
#   Default value: undef
#
# @param reportdirectory
#   Absolute path on the reporthost where the report should go.
#   Default value: undef
#
# @param admingroup
#   Absolute path on the reporthost where the report should go.
#   Default value: root
#
# @param program_paths
#   A hash containing alternative paths to use for backup programs. Valid keys
#   are: RDIFFBACKUP, CSTREAM, MYSQL, MYSQLHOTCOPY, MYSQLDUMP, PSQL, PGSQLDUMP,
#   PGSQLDUMPALL, GZIP and RSYNC. GZIP_OPTS can be used to define options to
#   use when GZIP is called.
#   Default: undef
#
# @param validate_actions
#   Whether to test run backup action configuration files before installing them.
#   If the test fails, the old configuration file is kept in place, or is not
#   installed, if it didn't exist.
#   Default value: false
#
# @param manage_packages
#   Whether to manage backupninja and handler-related packages.
#   Default value: true
#
# @param ensure_backupninja_version
#   Specifies the version of backupninja to install, or 'installed' for any version.
#   Default value: 'installed'
#
# @param ensure_borgbackup_version
#   Specifies the version of borgbackup to install, or 'installed' for any version.
#   Default value: 'installed'
#
# @param ensure_rsync_version
#   Specifies the version of rsync to install, or 'installed' for any version.
#   Default value: 'installed'
#
# @param ensure_rdiffbackup_version
#   Specifies the version of rdiff-backup to install, or 'installed' for any version.
#   Default value: 'installed'
#
# @param ensure_duplicity_version
#   Specifies the version of duplicity to install, or 'installed' for any version.
#   Default value: 'installed'
#
# @param selections_package_name
#   Specifies the name of the package providing debconf-get-selections (on Debian)
#   Default value: 'debconf-utils'
#
# @param hwinfo_package_name
#   Specifies the name of the package providing hwinfo
#   Default value: 'hwinfo'
#
# @param sfdisk_package_name
#   Specifies the name of the package providing sfdisk
#   Default value: 'sfdisk'
#
# @param cryptsetup_package_name
#   Specifies the name of the package providing cryptsetup
#   Default value: 'cryptsetup-bin'
#
# @param flashrom_package_name
#   Specifies the name of the package providing flashrom
#   Default value: 'flashrom'
#
# @param mysql_configfile
#   Path representing the MySQL client configuration file to use.
#   Default value: '/etc/mysql/debian.cnf'
#
# @param manage_cron
#   Whether to manage the default backupninja cron job.
#   Default value: true
#
# @param cron_ensure
#   Whether to ensure the presence or absence of the cron job.
#   Default value: present
#
# @param cron_file
#   Absolute path to the default cron job.
#   Default value: /etc/cron.d/backupninja
#
# @param cron_test_command
#   Specifies a test that must succeed for backupninja to run.
#   Default value: [ -x $backupninja::executable ]
#
# @param cron_command
#   Specifies the command to run in the cron job.
#   Default value: $backupninja::executable
#
# @param cron_command_options
#   Specifies options to append to the cron_command
#   Default value: undef
#
# @param cron_min
#   Specifies the minute component of the default cron job.
#   Default value: '0'
#
# @param cron_hour
#   Specifies the hour component of the default cron job.
#   Default value: '*'
#
# @param cron_dom
#   Specifies the day of month component of the default cron job.
#   Default value: '*'
#
# @param cron_month
#   Specifies the month component of the default cron job.
#   Default value: '*'
#
# @param cron_dow
#   Specifies the day of week component of the default cron job.
#   Default value: '*'
#
# @param server
#   Whether to set up this node to receive backups from other nodes.
#   Default value: false
#
# @param server_backupdir
#   Where to host backups on this node.
#   Default value: '/backup'
#
# @param server_backupdir_ensure
#   Whether the backupdir should be a directory or symlink.
#   Default value: 'directory'
#
# @param server_backupdir_target
#   If the backupdir is a symlink, where should it point to.
#   Default value: undef
#
# @param server_backupdir_mode
#   Defines the permissions for the backupdir.
#   Default value: undef
#
# @param account_host
#   Specifies the destination server configured by handlers by default (although
#   can be overridden by handler configuration).  Additionally, if this is set
#   to the $::fqdn fact of the backupserver, then client accounts will be
#   created, if left undefined, no account will be declared.
#   Default value: undef
#
# @param account_home
#   Specifies the absolute path which will be used as the home directory.
#   Default value: `"${backupninja::server_backupdir}/${::fqdn}"`
#
# @param account_user
#   Specifies the username of the client account.
#   Default value: `"backup-${::hostname}"`
#
# @param account_group
#   Specifies the group name which will server as gid for the server_backupdir
#   and the primary gid of the client account.
#   Default value: 'backupninjas'
#
# @param account_gid
#   Specifies the numeric gid of the account_group.
#   Default value: 700
#
# @param manage_account_homedir
#   Whether to create the account home directory on the server.
#   Default value: true
#
# @param manage_account_sshdir
#   Whether to create the client account's ssh configuration directory.
#   Default value: true
#
# @param manage_account_sshkey
#   Whether to deploy an ssh public key to the account's authorized_keys file.
#   Default value: true
#
# @param account_sshdir
#   Specifies the absolute path to the account's ssh configuration directory.
#   Default value: `"${backupninja::server_backupdir}/${::fqdn}/.ssh"`
#
# @param account_sshfile
#   Specifies the file name to the account's ssh authorized_keys file.
#   Default value: `"authorized_keys"`
#
# @param account_sshkey
#   Specifies the ssh public key to deploy in the account's authorized_keys
#   file. Must not contain key-type string (eg. ssh-rsa) or comment.
#   Default value: undef
#
# @param account_sshkey_type
#   Specifies the ssh public key type present in account_sshkey.
#   Default value: undef
#
# @param account_sshkey_options
#   Specifies key options deployed in the account's authorized_key file. This
#   may be used to configure restrictions on which commands and ssh features
#   the backup client has access to on the server.
#   Default value: undef
#
# @param account_sshkey_source
#   As an alternative to specifying a public key, type and options, this
#   parameter may be used to deploy the backup client's authorized_key file.
#   Default value: undef
#
#
class backupninja (
  Boolean $manage_packages,
  String $ensure_backupninja_version,
  String $ensure_borgbackup_version,
  String $ensure_rsync_version,
  String $ensure_rdiffbackup_version,
  String $ensure_duplicity_version,
  Stdlib::Absolutepath $configdirectory,
  Boolean $purge_configdirectory,
  Stdlib::Absolutepath $executable,
  Stdlib::Absolutepath $configfile,
  Integer $loglvl,
  String $when,
  String $reportemail,
  Boolean $reportinfo,
  Boolean $reportsuccess,
  Boolean $reportwarning,
  Boolean $reportspace,
  Optional[String] $reporthost,
  Optional[String] $reportuser,
  Optional[Stdlib::Absolutepath] $reportdirectory,
  String $admingroup,
  Stdlib::Absolutepath $logfile,
  Stdlib::Absolutepath $scriptdirectory,
  Stdlib::Absolutepath $libdirectory,
  Boolean $usecolors,
  Hash $program_paths,
  Boolean $manage_cron,
  Stdlib::Absolutepath $cron_file,
  Enum['present', 'absent'] $cron_ensure,
  Optional[String] $cron_test_command,
  Optional[String] $cron_command,
  Optional[String] $cron_command_options,
  String $cron_min,
  String $cron_hour,
  String $cron_dom,
  String $cron_month,
  String $cron_dow,
  Boolean $server,
  Optional[String] $server_backupdir,
  Enum['directory', 'link'] $server_backupdir_ensure,
  Optional[Stdlib::Absolutepath] $server_backupdir_target,
  Optional[Stdlib::Filemode] $server_backupdir_mode,
  Optional[String] $account_host,
  Optional[String] $account_home,
  Optional[String] $account_user,
  String $account_group,
  Integer $account_gid,
  Boolean $manage_account_homedir,
  Boolean $manage_account_sshdir,
  Boolean $manage_account_sshkey,
  Optional[String] $account_sshdir,
  Optional[String] $account_sshfile,
  Optional[String] $account_sshkey,
  Optional[String] $account_sshkey_type,
  Optional[Variant[String, Array[String]]] $account_sshkey_options,
  Optional[String] $account_sshkey_source,
  Optional[String] $mysql_configfile,
  Optional[String] $selections_package_name,
  Optional[String] $hwinfo_package_name,
  Optional[String] $sfdisk_package_name,
  Optional[String] $cryptsetup_package_name,
  Optional[String] $flashrom_package_name,
  Boolean $validate_actions,
) {

  contain backupninja::install
  contain backupninja::config
  contain backupninja::cron

  Class['backupninja::install']
  -> Class['backupninja::config']
  -> Class['backupninja::cron']

  if $server {
    include backupninja::server
  }

  if !empty($account_host) {

    # defaults based on other class parameters
    # must be defined in the manifest instead of hiera

    $_account_home = $account_home ? {
      undef   => "${server_backupdir}/${::fqdn}",
      default => $account_home
    }

    $_account_sshdir = $account_sshdir ? {
      undef   => "${_account_home}/.ssh",
      default => $account_sshdir
    }

    @@backupninja::server::account { "${::fqdn}@${account_host}":
      user           => $account_user,
      gid            => $account_group,
      home           => $_account_home,
      manage_homedir => $manage_account_homedir,
      manage_sshdir  => $manage_account_sshdir,
      manage_sshkey  => $manage_account_sshkey,
      sshdir         => $_account_sshdir,
      sshfile        => $account_sshfile,
      sshkey         => $account_sshkey,
      sshkey_type    => $account_sshkey_type,
      sshkey_options => $account_sshkey_options,
      sshkey_source  => $account_sshkey_source,
      tag            => $account_host,
    }
  }
  else {
    $_account_home = undef
    $_account_sshdir = undef
  }

}
